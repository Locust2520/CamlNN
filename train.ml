open Nntools;;

let (arch, datapath, iterations, nsample, learning_rate, outpath, loadbrain) = read_configfile "/media/fabien/LOCUST2520/TIPE/OcamlNN/trainconfig.txt";;

let dataset = datasetfromfile datapath;;

let brain = if (String.length loadbrain) == 0 then get_brain arch
				else open_brain loadbrain;;

print_string "Temps estimé en minutes : ";;
print_float (estimate_train_time brain dataset nsample iterations learning_rate);;
print_newline();;

print_string "Entraînement...";;
print_newline();;

for i = 1 to iterations do
    print_string "Iteration : ";
    print_int i;
    print_newline();
	let dataset_ = rand_sample dataset nsample in
	train brain dataset_ learning_rate;
    print_string "Erreur moyenne sur la base de données : ";
    print_float (error_mean brain dataset);
    print_newline();
    if (i mod save_every) == 0 then (
        print_string (String.concat "" ["Sauvegarde dans " ; outpath ; (string_of_int i) ; "..."]);
        print_newline();
        save_brain brain (String.concat "" [outpath ; (string_of_int i)]);
    )
done;;

print_string "Taux de réussite sur la base de données : ";;
print_float (accuracy brain dataset);;
print_newline();;
print_string "Erreur moyenne sur la base de données : ";;
print_float (error_mean brain dataset);;
print_newline();;
print_string (String.concat "" ["Sauvegarde dans " ; outpath ; "..."]);;
print_newline();;
save_brain brain outpath;;
