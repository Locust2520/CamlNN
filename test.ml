open Nntools;;

let (brainpath, datapath, outpath) = read_testconfigfile "/media/fabien/LOCUST2520/TIPE/OcamlNN/testconfig.txt";;

let dataset = datasetfromfile datapath;;

let brain = open_brain brainpath;;

print_string "Taux de réussite sur la base de données : ";;
print_float (accuracy brain dataset);;
print_newline();;
print_string "Erreur moyenne sur la base de données : ";;
print_float (error_mean brain dataset);;
print_newline();;
if (String.length outpath) > 0 then (
	print_string (String.concat "" ["Sauvegarde des résultats du réseau dans " ; outpath ; "...\r"]);
	let file = open_out outpath in
	write_brain_outputs brain dataset file;
	close_out file
	);;