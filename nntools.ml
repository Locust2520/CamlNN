(* power function *)
let rec pow a = function
  | 0 -> 1
  | n -> a*(pow a (n-1));;

(* sigmoid function *)
let sigm = fun x -> 1.0 /. (1.0 +. exp(-. x));;

(* sigmoid derivative function *)
let dsigm = fun x -> let s = sigm x in s *. (1. -. s);;

(* function which returns a random number between -1 and 1 *)
let rand = fun x -> Random.float 2.0 -. 1.;;

(* returns a matrix of size (n, m) with random terms between 0 and 1 *)
let randmat (n, m) = Array.map (Array.map rand) (Array.make_matrix n m 0.) ;; 

(* returns a mapped matrix with the sigmoid function *)
let matsigm m = Array.map (Array.map sigm) m;;

(* returns a mapped matrix with the derivative sigmoid function *)
let matdsigm m = Array.map (Array.map dsigm) m;;

(* returns the matrix multiplication of a and b *)
let matmul (a, b) =
	let c = Array.make_matrix (Array.length a) (Array.length b.(0)) 0. in
	for i = 0 to (Array.length a)-1 do
		for j = 0 to (Array.length b.(0))-1 do
			for k = 0 to (Array.length b)-1 do
				c.(i).(j) <- c.(i).(j) +. a.(i).(k) *. b.(k).(j)
			done
		done
	done ;
	c ;;

(* returns (a_ij * b_ij) if (a_ij) and (b_ij) are given*)
let matmul2 (a, b) =
	for i = 0 to (Array.length b)-1 do
		for j = 0 to (Array.length b.(0))-1 do
			a.(i).(j) <- a.(i).(j) *. b.(i).(j)
		done
	done ;
	a ;;

(* returns the scalar multiplication lambda.m *)
let scalmul (l, m) =
	for i = 0 to (Array.length m)-1 do
		for j = 0 to (Array.length m.(0))-1 do
			m.(i).(j) <- l *. m.(i).(j)
		done
	done ;
	m ;;

(* matrix addition *)
let matadd (a, b) =
	for i = 0 to (Array.length b)-1 do
		for j = 0 to (Array.length b.(0))-1 do
			a.(i).(j) <- a.(i).(j) +. b.(i).(j)
		done
	done ;
	a ;;

(* matrix subtraction *)
let matsub (a, b) =
	for i = 0 to (Array.length b)-1 do
		for j = 0 to (Array.length b.(0))-1 do
			a.(i).(j) <- a.(i).(j) -. b.(i).(j)
		done
	done ;
	a ;;

(* matrix transposition *)
let transpose m =
	let n = Array.length m
	and p = Array.length m.(0) in
	let mt = Array.make_matrix p n 0. in
	for i = 0 to n-1 do
		for j = 0 to p-1 do
			mt.(j).(i) <- m.(i).(j)
		done;
	done;
	mt;;

(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ config file functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)

(* splits the string str with the separator ch into a list *)
let rec split str ch = 
	if String.contains str ch then
		let i = String.index str ch in
		(String.sub str 0 i) :: split (String.sub str (i+1) ((String.length str)-i-1)) ch
	else str :: [] ;;

(* converts a list of strings into an array of integers *)
let rec strlist_to_inttarray lst =
	if List.length lst == 0 then [||]
	else
		try Array.append [|int_of_string (List.hd lst)|] (strlist_to_inttarray (List.tl lst))
		with Failure "int_of_string" -> strlist_to_inttarray (List.tl lst);;

let str2arr = fun str -> strlist_to_inttarray (split str ';');;

let rec trim s =
  let l = String.length s in 
  if l=0 then s
  else if s.[l-1]='\r' then String.sub s 0 (l-1)
  else s;;

(* read lines of the config file *)
let read_file filename = 
	let lines = ref [] and file = open_in filename in
	try
		while true; do
			lines := !lines @ [trim (input_line file)]
		done; !lines
	with End_of_file ->
		close_in file;
		!lines ;;

(* returns all train configs we need. *)
let read_configfile filename = let architecture :: datapath :: iterations :: samples :: learning_rate :: outpath :: save_every :: brainload :: t = read_file filename in
	((str2arr architecture),
	datapath,
	(int_of_string iterations),
	(int_of_string samples),
	(float_of_string learning_rate),
	outpath,
    (int_of_string save_every),
	brainload);;


(* returns all test configs we need. *)
let read_testconfigfile filename = let brainpath :: datapath :: outpath :: t = read_file filename in
	(brainpath,
	datapath,
	outpath);;


(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ dataset functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)

(* gives a list of train data from a binary file *)
let datasetfromfile filename =
	let file = open_in_bin filename
	and inlength = ref 0
	and outlength = ref 0 in
	(* the 4 first bytes correspond to the input length *)
	for i = 0 to 3 do
		inlength := !inlength + (input_byte file)*(pow 256 (3-i))
	done;
	(* the newt 2 bytes are the output length *)
	for i = 0 to 1 do
		outlength := !outlength + (input_byte file)*(pow 256 (1-i))
	done;
	let n = (in_channel_length file)/(!inlength + !outlength)
	and dataset = ref []
	and input = ref (Array.make_matrix !inlength 1 0.)
	and output = ref (Array.make_matrix !outlength 1 0.) in
	for i = 1 to n do
		input := Array.make_matrix !inlength 1 0. ;
		output := Array.make_matrix !outlength 1 0. ;
		for i = 0 to (!inlength - 1) do
			!input.(i).(0) <- float_of_int (input_byte file) /. 255.
		done;
		for i = 0 to (!outlength - 1) do
			!output.(i).(0) <- float_of_int (input_byte file) /. 255.
		done;
		dataset := [!input ; !output]::(!dataset)
	done;
	close_in file;
	!dataset;;

let rec length = function
	| [] -> 0
	| h::t -> 1 + length t;;

let shuffle d =
    let nd = List.map (fun c -> (Random.bits (), c)) d in
    let sond = List.sort compare nd in
    List.map snd sond;;

let rand_sample dataset n =
	let new_ds = shuffle dataset
	and m = ref n and l = length dataset in
	if n > l then m := l;
	let n = !m in
	let rec slice = function
		| (_, 0) -> []
		| (h::t, n) -> h::(slice (t, (n-1)))
		| ([], _) -> []
	in slice (new_ds, n);;

(* returns the index of the biggest number in an array *)
let max_index arr =
	let i = ref 0
	and n = Array.length arr in
	for k = 0 to n-1 do
		if arr.(k) > arr.(!i) then i := k
	done;
	!i;;

(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ neural network's functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)

(* generates random weights, biases, potentials and layers by given a size vect *)
let get_brain size_vect =
	let n = Array.length size_vect in
	let layers = Array.make n (Array.make_matrix 1 1 0.)
	and weights = Array.make (n-1) (Array.make_matrix 1 1 0.)
	and biases = Array.make (n-1) (Array.make_matrix 1 1 0.)
	and potentials = Array.make (n-1) (Array.make_matrix 1 1 0.) in
	for i = 0 to (n-2) do
		layers.(i) <- randmat(size_vect.(n-i-1), 1);
		potentials.(i) <- randmat(size_vect.(n-i-1), 1);
		biases.(i) <- randmat(size_vect.(n-i-1), 1);
		weights.(i) <- randmat(size_vect.(n-i-1), size_vect.(n-i-2));
	done;
	layers.(n-1) <- randmat(size_vect.(0), 1);
	(weights, biases, potentials, layers);;

(* put an input vect into the brain *)
let set_input (weights, biases, potentials, layers) input =
	let n = Array.length layers in
	layers.(n-1) <- input;;

(* manages to sync all the layers from the input, weights and biases *)
let refresh_brain (weights, biases, potentials, layers) =
	let n = Array.length weights in
	for i = 1 to n do
		potentials.(n-i) <- matadd(matmul(weights.((n-i)), layers.((n-i+1)) ), biases.((n-i)));
		layers.((n-i)) <- matsigm(potentials.(n-i))
	done;;

(* get the output layer of a brain *)
let get_output (weights, biases, potentials, layers) =
	layers.(0);;

(* error function for a wanted layer and an obtained layer *)
let error (weights, biases, potentials, layers) target =
	let output = layers.(0) in
	let n = Array.length output
	and e = ref 0. in
	for i = 0 to (n-1) do
		e := !e +. (output.(i).(0) -. target.(i).(0)) *. (output.(i).(0) -. target.(i).(0))
	done ;
	0.5 *. !e ;;

(* error signals for the last layer *)
let error_signal_init potentials layer target = matmul2((matdsigm potentials), matsub(layer, target));;

(* gradient descent algorithm *)
let gradient_descent (weights, biases, potentials, layers) target step_size =
	let n = Array.length weights
	and s = ref (matmul2((matdsigm potentials.(0)), matsub(layers.(0), target))) in
	biases.(0) <- matsub(biases.(0), scalmul(step_size, !s));
	weights.(0) <- matsub(weights.(0), scalmul(step_size, matmul(!s, (transpose layers.(1)))));
	for i = 1 to n-1 do
		s := matmul2(matdsigm(potentials.(i)), matmul(weights.(i-1), !s));
		biases.(i) <- matsub(biases.(i), scalmul(step_size, !s));
		weights.(i) <- matsub(weights.(i), scalmul(step_size, matmul(!s, (transpose layers.(i+1)))));
	done
	;;

(* allows to train a given brain on a whole dataset (one iteration per sample) *)
let rec train brain dataset step_size = match dataset with
	| [] -> ()
	| data::tail -> let input = List.hd data and target = List.hd (List.tl data) in
							set_input brain input;
							refresh_brain brain;
							gradient_descent brain target step_size;
							train brain tail step_size;;

(* estimates the time needed for training *)
let estimate_train_time brain dataset nsamples iterations step_size =
	let t1 = Sys.time ()
	and data = List.hd dataset in
	let input = List.hd data and target = List.hd (List.tl data) in
		set_input brain input;
		refresh_brain brain;
		gradient_descent brain target step_size;
	(float_of_int (nsamples*iterations)) *. (Sys.time () -. t1) /. 60. ;;

(* error mean of a brain on a dataset *)
let error_mean brain dataset =
	let rec err_aux brain dataset =
		match dataset with
		| [] -> 0.
		| data::tail -> let input = List.hd data and target = List.hd (List.tl data) in
								set_input brain input;
								refresh_brain brain;
								(err_aux brain tail) +. (error brain target)
	in
	(err_aux brain dataset) /. (float_of_int (length dataset));;

(* accuracy of a brain on a dataset if we look at the biggest output *)
let accuracy brain dataset =
	let rec acc_aux brain dataset =
		match dataset with
		| [] -> 0
		| data::tail -> let input = List.hd data and target = List.hd (List.tl data) in
								set_input brain input;
								refresh_brain brain;
								if max_index (get_output brain) == max_index target then 1 + (acc_aux brain tail)
								else acc_aux brain tail
	in
	(float_of_int (acc_aux brain dataset)) /. (float_of_int (length dataset));;

(* write the output of the brain in a given channel_out *)
let write_brain_output (weights, biases, potentials, layers) file =
	let out = layers.(0) in
	let n = Array.length out in
	for i = 0 to (n-1) do
		output_string file (string_of_int (int_of_float (out.(i).(0) *. 256. )));
		output_string file ";"
	done;;

(* write the outputs of a brain with a given dataset and a channel_out *)
let rec write_brain_outputs brain dataset file =
	match dataset with
	| [] -> ()
	| data::tail -> let input = List.hd data in
								write_brain_outputs brain tail file;
								set_input brain input;
								refresh_brain brain;
								write_brain_output brain file;
								output_string file "|";;


(* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ neural network's opening and saving ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ *)

(* decomposition of an integer into a list of bytes *)
let rec decompose n nbytes =
	match nbytes with
	| 0 -> []
	| k -> (decompose (n/256) (k-1))@[n mod 256];;

(* write n bytes from an integer into a file *)
let rec writebytes file n nbytes =
	match nbytes with
	| 0 -> ()
	| k -> writebytes file (n/256) (k-1); output_byte file n;;

(* read n bytes from a file to make an integer *)
let rec readbytes file nbytes =
	match nbytes with
	| 0 -> 0
	| k -> (readbytes file (k-1)) + (input_byte file)*(pow 256 (k-1));;


(* write a float into 4 bytes (first byte is the exponent *)
let writefloat file f =
	let (m, e) = frexp f in
	if e < 0 then output_byte file (128 + (-e mod 128))
	else output_byte file (e mod 128);
	if m < 0. then writebytes file (8388608 + (int_of_float (-. m *. 8388608.))) 3
	else writebytes file (int_of_float (m *. 8388608.)) 3;;

(* read a float from 4 bytes *)
let readfloat file =
	let e1 = input_byte file
	and m3 = input_byte file
	and m2 = input_byte file
	and m1 = input_byte file
	and e = ref 0
	and m = ref 0. in
	if e1 >= 128 then e := -e1 + 128
	else e := e1;
	if m3 >= 128 then m := ((float_of_int ((-m3 + 128)*65536 - m2*256 - m1)) /. 8388608.)
	else m := ((float_of_int (m3*65536 + m2*256 + m1)) /. 8388608.);
	ldexp !m !e;;

(* write a 2-dimensional array into a file *)
let writematrix file mat =
	let n = Array.length mat
	and p = Array.length mat.(0) in
	for i = 0 to (n-1) do
		for j = 0 to (p-1) do
			writefloat file mat.(i).(j)
		done;
	done;;

(* read a 2-dimensional array of size (n, p) into a file *)
let readmatrix file (n,p) =
	let mat = Array.make_matrix n p 0. in
	for i = 0 to (n-1) do
		for j = 0 to (p-1) do
			mat.(i).(j) <- readfloat file
		done;
	done;
	mat;;


(* save the brain into a .brain file *)
let save_brain (weights, biases, potentials, layers) filename =
	let file = open_out_bin filename
	and n = Array.length weights in
	output_byte file n;
	for i = 0 to n do
		writebytes file (Array.length layers.(i)) 4
	done;
	for i = 0 to (n-1) do
		writematrix file weights.(i);
		writematrix file biases.(i)
	done;
	close_out file;;

(* open a brain from a .brain file *)
let open_brain filename =
	let file = open_in_bin filename in
	let n = input_byte file in
	let arch = Array.make (n+1) 0 in
	for i = 0 to n do
		arch.(n-i) <- readbytes file 4
	done;
	let (weights, biases, potentials, layers) = get_brain arch in
	for i = 0 to (n-1) do
		weights.(i) <- readmatrix file (arch.(n-i), arch.(n-i-1));
		biases.(i) <- readmatrix file (arch.(n-i), 1)
	done;
	close_in file;
	(weights, biases, potentials, layers);;
